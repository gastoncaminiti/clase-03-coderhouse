using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public int lifePlayer = 3;
    public string namePlayer = "Mr. Blue";
    float speedPlayer = 100.0f;
    public Vector3 initPosition = new Vector3(4, 2, 1);
    public GameObject swordPlayer; 
    // Start is called before the first frame update
    void Start()
    {
        
       // Debug.Log("EN EL FRAME INICIAL...");
       // Debug.Log(namePlayer);
       // Debug.Log(lifePlayer);
        transform.position =initPosition;
        transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        //  Debug.Assert(lifePlayer > 0, "LAS VIDAS TIENEN QUE SER MAYOR QUE 0");
        //Debug.Break();
        Debug.Log(swordPlayer.GetComponent<SwordController>().GetSwordName());
        swordPlayer.GetComponent<SwordController>().SetSwordName("ESPADON 9000");
        Debug.Log(swordPlayer.GetComponent<SwordController>().GetSwordName());
        swordPlayer.transform.position = transform.position + new Vector3(0,0,0.3f);

    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("EN CADA FRAME...");
    }
}
